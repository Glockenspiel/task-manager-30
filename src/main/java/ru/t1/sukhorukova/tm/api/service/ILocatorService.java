package ru.t1.sukhorukova.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILocatorService {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

}
