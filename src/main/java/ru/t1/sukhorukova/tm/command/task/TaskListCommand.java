package ru.t1.sukhorukova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sukhorukova.tm.enumerated.TaskSort;
import ru.t1.sukhorukova.tm.model.Task;
import ru.t1.sukhorukova.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    public static final String DESCRIPTION = "Show task list.";

    @Override
    public void execute() {
        System.out.println("[TASK LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(TaskSort.values()));
        @Nullable final String sortType = TerminalUtil.nextLine();

        @NotNull final String userId = getAuthService().getUserId();
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        @Nullable final Comparator<Task> comparator = sort != null ? sort.getComparator() : null;
        @NotNull final List<Task> tasks = getTaskService().findAll(userId, comparator);

        renderTasks(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
